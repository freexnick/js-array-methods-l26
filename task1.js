/* ამოცანა #1
Make a program that filters a list of strings and returns a list with only your friends name in it. If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise, you can be sure he's not...
Ex: Input = ["Ryan", "Kieran", "Jason", "Yous"], Output = ["Ryan", "Yous"]

friend ["Ryan", "Kieran", "Mark"] shouldBe ["Ryan", "Mark"]
ტესტ ქეისები (ამ მასივებზე შეგიძლიათ გატესტოთ ფუნქცია)
["George", "Nick", "Tom", "Kate", "Annie"] უნდა დააბრუნოს ["Nick", "Kate"]
["James", "Will", "Jack", "Nate", "Edward"] უნდა დააბრუნოს ["Will", "Jack", "Nate"]*/

const people = [
  { list: ["George", "Nick", "Tom", "Kate", "Annie"] },
  { list: ["James", "Will", "Jack", "Nate", "Edward"] },
];

const findFriends = (arr) => {
  return arr.filter((person) => person.length === 4);
};

for (person of people) {
  person.friends = findFriends(person.list);
}

console.log(people);
