/*ამოცანა #2
Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers. No floats or non-positive integers will be passed.
For example, when an array is passed like [19, 5, 42, 2, 77], the output should be 7.
[10, 343445353, 3453445, 3453545353453] should return 3453455. 

ტესტ ქეისები (ამ მასივებზე შეგიძლიათ გატესტოთ ფუნქცია)
[5, 8, 12, 19, 22] უნდა დააბრუნოს 5+8 ის ჯამი (13)
[52, 76, 14, 12, 4] უნდა დააბრუნოს 4 + 12 ის ჯამი (16)
[3, 87, 45, 12, 7] უნდა დააბრუნოს 3 + 7 ის ჯამი (10)
*/

const lists = [
  { integerList: [5, 8, 12, 19, 22] },
  { integerList: [52, 76, 14, 12, 4] },
  { integerList: [3, 87, 45, 12, 7] },
];

const calcTwoLowestPositives = (arr) => {
  const result = arr.sort((num1, num2) => num1 - num2);
  return result[0] + result[1];
};

for (list of lists) {
  list.sumOfTwoLowestPositives = calcTwoLowestPositives(list.integerList);
}

console.log(lists);
